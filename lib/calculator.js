const exitCodes = require('./exitCodes');

function VAT (amount) {
  return amount * 0.23;
}

function subtotal (catalog, cart) {
  let subtotal = 0;

  for (let i = 0; i < cart.length; i++) {
    const cartItem = cart[i];

    const hasStock = catalog[cartItem.sku] && catalog[cartItem.sku].amount > 0;
    if (!hasStock) throw new Error(exitCodes.OUT_OF_STOCK);

    const hasEnoughStock = catalog[cartItem.sku].amount >= cartItem.amount;
    if (!hasEnoughStock) throw new Error(exitCodes.NOT_ENOUGH_STOCK);

    subtotal += (catalog[cartItem.sku].price * cartItem.amount);
  }
  
  return subtotal;
}

function total (catalog, cart) {
  const subtotal = this.subtotal(catalog, cart);
  return subtotal + this.VAT(subtotal);
}

module.exports = {
  VAT: VAT,
  subtotal: subtotal,
  total: total
};
