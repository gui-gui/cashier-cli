const pJson = require('../package.json'),
      reader = require('./reader'),
      parser = require('./parser'),
      calculator = require('./calculator'),
      output = require('./output');

const Cashier = new Function();

Cashier.prototype.version = function() {
  return ('Cashier version: ' + pJson.version);
}

Cashier.prototype.reader = function(file) {
  return reader(file);
}

Cashier.prototype.parseCatalog = function(data) {
  return parser.catalog(data);
}

Cashier.prototype.parseCart = function(items) {
  return parser.cart(items);
}

Cashier.prototype.calculateTotal = function(catalog, cart) {
  return calculator.total(catalog, cart);
}

Cashier.prototype.run = function(commands) {
  const self = this;
  const catalogFile = commands[2];
  const cartItems = commands.slice(3);

  const catalog = self.parseCatalog(self.reader(catalogFile));
  const cart = self.parseCart(cartItems);

  const total = self.calculateTotal(catalog, cart);

  return output.orderTotal(total);
}

module.exports = new Cashier();