const Cashier = require('./cashier'),
      logger = require('./logger'),
      verify = require('./utils').verify,
      commands = process.argv;

if (verify(['-v', '--version'])) {
  console.log(Cashier.version());
} else {
  try {
    console.log(Cashier.run(commands));
  } catch(error) {
    logger(error);
  }
}