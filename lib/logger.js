function log (error) {
  let message;
  
  if (error.message == 1) {
    message = '(1) ERROR: Catalog does not have enough inventory in stock to fullfil this order';
  } else {
    message = 'ERROR: Cashier crashed! ' + error.message;
  }
  
  console.log(message);
  process.exitCode = error.message;
}

module.exports = log;
