function orderTotal(amount) {
  const formattedAmount = (amount/100).toFixed(2).replace('.', ',');
  return 'Total: ' + formattedAmount;
}

module.exports = {
  orderTotal: orderTotal
};
