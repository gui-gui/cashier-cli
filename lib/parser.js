const Product = require('./product');

function Catalog (data) {
  let catalog = {};
  const lines = data.match(/[^\r\n]+/g);

  lines.forEach(line => {
    const item = line.split(',');
    catalog[item[0]] = new Product(item[0], item[1], item[2]);
  });
  
  return catalog;
}

function Cart (items) {
  let cart = [];

  for(let i = 0; i < items.length; i += 2) {
    cart.push(new Product(items[i], items[i+1] || 0))
  }

  return cart;
}

module.exports = {
  catalog: Catalog,
  cart: Cart
};
