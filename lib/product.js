const exitCodes = require('./exitCodes');

function Product(sku, amount, price) {
  if(typeof sku !== 'string' || 
    (typeof amount !== 'string' && typeof amount !== 'number') ||
    (price && typeof price !== 'string' && typeof price !== 'number'))  {
    throw new Error(exitCodes.GENERIC);
  }

  this.sku = sku;
  this.amount = Number(amount);
  this.price = null;
  
  if (typeof price === 'number') this.price = price;
  if (typeof price === 'string') this.price = Number(price.replace(/\D/g, ""));
}

module.exports = Product;