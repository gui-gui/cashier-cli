const calculator = require('../lib/calculator');
const reader = require('../lib/reader');
const parser = require('../lib/parser');

describe('The Calculator', () => {
  it('should calculate VAT', () => {
    expect(calculator.VAT(100)).toBe(23);
    expect(calculator.VAT(1000)).toBe(230);
    expect(calculator.VAT(10000)).toBe(2300);
  })
  
  describe('subtotal', () => {
    it('should calculate the subtotal', () => {
      const catalog = reader('./tests/fixtures/catalog-1.csv');
      const parsedCatalog = parser.catalog(catalog);
      const cart = parser.cart(['P1', '1', 'P2', '1']);
      const cart2 = parser.cart(['P1']);
      const cart3 = parser.cart(['P1', '1', 'P2', '4']);
      const cart4 = parser.cart(['P1', '5']);
      expect(calculator.subtotal(parsedCatalog, cart)).toBe(125000);
      expect(calculator.subtotal(parsedCatalog, cart2)).toBe(0);
      expect(calculator.subtotal(parsedCatalog, cart3)).toBe(200000);
      expect(calculator.subtotal(parsedCatalog, cart4)).toBe(500000);
    })

    it('should throw error code 1 if out of stock', () => {
      const catalog = reader('./tests/fixtures/catalog-2.csv');
      const parsedCatalog = parser.catalog(catalog);
      const cart = parser.cart(['P10', '2']);
      expect(() => calculator.subtotal(parsedCatalog, cart)).toThrowError("1");
    })
        
    it('should throw error code 1 if not enough inventory in stock', () => {
      const catalog = reader('./tests/fixtures/catalog-2.csv');
      const parsedCatalog = parser.catalog(catalog);
      const cart = parser.cart(['P1', '2']);
      expect(() => calculator.subtotal(parsedCatalog, cart)).toThrowError("1");
    })
  })

  describe('total', () => {
    it('should calculate the total', () => {
      const catalog = reader('./tests/fixtures/catalog-1.csv');
      const parsedCatalog = parser.catalog(catalog);
      const cart = parser.cart(['P1', '1', 'P2', '1']);
      const cart2 = parser.cart(['P1']);
      const cart3 = parser.cart(['P1', '1', 'P2', '4']);
      const cart4 = parser.cart(['P1', '5']);
      
      expect(calculator.total(parsedCatalog, cart)).toBe(153750);
      expect(calculator.total(parsedCatalog, cart2)).toBe(0);
      expect(calculator.total(parsedCatalog, cart3)).toBe(246000);
      expect(calculator.total(parsedCatalog, cart4)).toBe(615000);
    })

    it('should throw error code 1 if out of stock', () => {
      const catalog = reader('./tests/fixtures/catalog-2.csv');
      const parsedCatalog = parser.catalog(catalog);
      const cart = parser.cart(['P10', '2']);
      expect(() => calculator.subtotal(parsedCatalog, cart)).toThrowError("1");
    })

    it('should throw error code 1 if not enough inventory in stock', () => {
      const catalog = reader('./tests/fixtures/catalog-2.csv');
      const parsedCatalog = parser.catalog(catalog);
      const cart = parser.cart(['P1', '2']);
      expect(() => calculator.total(parsedCatalog, cart)).toThrowError("1");
    })  
  })
})