const Cashier = require('../lib/cashier'),
      Product = require('../lib/product'),
      fs = require('fs');

describe('The Cashier instance', () => {
  it('should have a version method', () => expect(typeof Cashier.version).toBe('function'))
  it('should have a reader method', () => expect(typeof Cashier.reader).toBe('function'))
  it('should have a parseCatalog method', () => expect(typeof Cashier.parseCatalog).toBe('function'))
  it('should have a parseCart method', () => expect(typeof Cashier.parseCart).toBe('function'))
  it('should have a calculateTotal method', () => expect(typeof Cashier.calculateTotal).toBe('function'))
  it('should have a run method', () => expect(typeof Cashier.run).toBe('function'))

  describe('version method', () => {
    it('should be equal to package.json version', () => {
      const packageVersion = require('../package.json').version,
            cashierVersion = Cashier.version();
      
      expect(cashierVersion).toBe('Cashier version: ' + packageVersion);
    })
  })

  describe('reader method', () => {
    it('should read the file', () => {
      const file1 = fs.readFileSync(__dirname + '/../tests/fixtures/catalog-1.csv', 'utf8');
      const file2 = fs.readFileSync(__dirname + '/../tests/fixtures/catalog-2.csv', 'utf8');
      expect(Cashier.reader('./tests/fixtures/catalog-1.csv')).toEqual(file1);
      expect(Cashier.reader('./tests/fixtures/catalog-2.csv')).toEqual(file2);
    })
  })

  describe('parseCatalog method', () => {
    it('should return the parsed catalog', () => {
      const catalog = Cashier.reader('./tests/fixtures/catalog-1.csv');
      const P1 = new Product('P1', '5', '1000.00');
      const P2 = new Product('P2', '8', '250.00');
      expect(Cashier.parseCatalog(catalog)).toHaveProperty('P1', P1);
      expect(Cashier.parseCatalog(catalog)).toHaveProperty('P2', P2);
    })
  })

  describe('parseCart method', () => {
    it('should return parsed cart', () => {
      const P1 = new Product('P1', '2');
      const P2 = new Product('P2', '3');
      expect(Cashier.parseCart(['P1', '2', 'P2', '3']))
        .toEqual(expect.arrayContaining([P1, P2]));
    })
  })

  describe('calculateTotal method', () => {
    it('should return the cart total', () => {
      const catalog = Cashier.reader('./tests/fixtures/catalog-1.csv');
      const parsedCatalog = Cashier.parseCatalog(catalog);
      const cart1 = Cashier.parseCart(['P1', '1']);
      const cart2 = Cashier.parseCart(['P2', '2']);

      expect(Cashier.calculateTotal(parsedCatalog, cart1)).toEqual(123000);
      expect(Cashier.calculateTotal(parsedCatalog, cart2)).toEqual(61500);
    })

    it('should throw error code 1 if not enough inventory in stock', () => {
      const catalog = Cashier.reader('./tests/fixtures/catalog-2.csv');
      const parsedCatalog = Cashier.parseCatalog(catalog);
      const cart = Cashier.parseCart(['P1', '2']);

      expect(() => Cashier.calculateTotal(parsedCatalog, cart)).toThrowError('1');
    })
  })

  describe('run method', () => {
    it('should output the total of the order', () => {
      const commands1 = [null,null, './tests/fixtures/catalog-1.csv', 'P1', '1'];
      const commands2 = [null,null, './tests/fixtures/catalog-1.csv', 'P1', '2', 'P2', '4'];

      expect(Cashier.run(commands1)).toBe('Total: 1230,00');
      expect(Cashier.run(commands2)).toBe('Total: 3690,00');
    });
    
    it('should throw error code 1 if out of stock', () => {
      const commands1 = [null,null, './tests/fixtures/catalog-2.csv', 'P10', '2'];
      const commands2 = [null,null, './tests/fixtures/catalog-2.csv', 'P6', '1'];
      expect(() => Cashier.run(commands1)).toThrowError('1');
      expect(() => Cashier.run(commands2)).toThrowError('1');
    })

    it('should throw error code 1 if not enough inventory in stock', () => {
      const commands = [null,null, './tests/fixtures/catalog-2.csv', 'P1', '2'];
      expect(() => Cashier.run(commands)).toThrowError('1');
    })
  })
})