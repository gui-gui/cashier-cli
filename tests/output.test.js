const output = require('../lib/output');

it('should print correct total output', () => {
  expect(typeof output.orderTotal(100)).toBe('string');
  expect(output.orderTotal(0)).toBe('Total: 0,00');
  expect(output.orderTotal(1)).toBe('Total: 0,01');
  expect(output.orderTotal(10)).toBe('Total: 0,10');
  expect(output.orderTotal(100)).toBe('Total: 1,00');
  expect(output.orderTotal(1000)).toBe('Total: 10,00');
  expect(output.orderTotal(10000)).toBe('Total: 100,00');
  expect(output.orderTotal(100000)).toBe('Total: 1000,00');
})