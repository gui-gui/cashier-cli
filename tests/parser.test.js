const reader = require('../lib/reader');
const parser = require('../lib/parser');
const Product = require('../lib/product');

describe('The catalog parser', () => {
  it('should return an object that contains products', () => {
    const catalog = reader('./tests/fixtures/catalog-1.csv');
    const parsedCatalog = parser.catalog(catalog);
    expect(typeof parsedCatalog).toBe('object');
    for(let key in parsedCatalog) {
      expect(parsedCatalog[key]).toBeInstanceOf(Product);
    }
  })
})

describe('The cart parser', () => {
  it('should return an array of products', () => {
    const cart = parser.cart(['P1', '2', 'P3', '4']);
    expect(Array.isArray(cart)).toBe(true);
    expect(cart[0].sku).toBe('P1');
    cart.forEach(item => {
      expect(item).toBeInstanceOf(Product);
    })
  })
})