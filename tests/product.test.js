const Product = require('../lib/product');

describe('The Product instance', () => {
  it('should expose sku, amount and price attributes', () => {
    const P1 = new Product('P1', '1000', '10.00');
    expect(P1.sku).toBeTruthy();
    expect(P1.amount).toBeTruthy();
    expect(P1.price).toBeTruthy();
  })

  it('should always have a sku', () => {
    const P1 = new Product('P1', '10', '50.00');
    const P2 = new Product('P2', '5', '10.00');
    expect(typeof P1.sku).toBe('string');
    expect(P1.sku).toBe('P1');
    expect(P2.sku).toBe('P2');
    expect(() => new Product()).toThrow();
  })

  it('should always have an amount', () => {
    const P1 = new Product('P1', '5', '100.00');
    expect(typeof P1.amount).toBe('number');
    expect(P1.amount).toBe(5);
    expect(() => new Product('Product')).toThrow();
  })

  it('should have a valid price or be null price', () => {
    const P1 = new Product('P1', '1', '10.00');
    const P2 = new Product('P2', '2');
    const P3 = new Product('P1', '1', 555);
    expect(typeof P1.price).toBe('number');
    expect(P1.price).toBe(1000);
    expect(P2.price).toBeNull();
    expect(P3.price).toBe(555);
  })
})